package com.example.proyecto

import android.Manifest
import android.Manifest.permission.*
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_visualiza_publicacion.*


class visualiza_publicacion:AppCompatActivity() {
    // Crea instancia de la base de datos para ser utilizada
    // Create local variables but not initialized
    lateinit var fiba: DatabaseReference
    lateinit var fiba2: DatabaseReference

    lateinit var lstdatos: MutableList<clsUsuario>
    lateinit var token: String

    lateinit var  descripcion_VP: TextView
    lateinit var  provincia_VP:TextView
    lateinit var  canton_VP:TextView
    lateinit var  categoria_VP:TextView
    lateinit var  subcategoria_VP:TextView
    lateinit var  telefono_VP:TextView


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_visualiza_publicacion)

        fiba = FirebaseDatabase.getInstance().getReference("publicaciones")
        fiba2 = FirebaseDatabase.getInstance().getReference("usuario")

        lstdatos = mutableListOf()
        token = ""
        var id = intent.extras!!.get("id")!!.toString()

        descripcion_VP=  this.findViewById(R.id.descripcion_vp) as TextView
        provincia_VP=  this.findViewById(R.id.provincia_vp) as TextView
        canton_VP=  this.findViewById(R.id.canton_vp) as TextView
        categoria_VP=  this.findViewById(R.id.categoria_vp) as TextView
        subcategoria_VP=  this.findViewById(R.id.subcategoria_vp) as TextView
        telefono_VP=  this.findViewById(R.id.telefono_vp) as TextView

        obtienePublicacion(id)


        btnLlamar.setOnClickListener {
            haceLlamadas(telefono_VP.getText().toString())
        }
    }

    fun haceLlamadas(numeroTel:String){
        val intent = Intent(Intent.ACTION_CALL)
        intent.data=Uri.parse("tel:$numeroTel")
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)==PackageManager.PERMISSION_GRANTED){
            startActivity(intent)
        }else{
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CALL_PHONE), 0)
        }
    }

    fun obtienePublicacion(id: String) {


        fiba.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // Clear data list

                if (dataSnapshot.exists()) {
                    // Execute read data container and populate local list as clsDato instance
                    for (data in dataSnapshot.children) {
                        val idBD = data.child("id").getValue().toString()

                        if (id == idBD) {
                            descripcion_VP.text=data.child("descripcion").getValue().toString()
                            provincia_VP.text=data.child("provincia").getValue().toString()
                            canton_VP.text=data.child("canton").getValue().toString()
                            categoria_VP.text=data.child("categoria").getValue().toString()
                            subcategoria_VP.text=data.child("subcategoria").getValue().toString()

                            obtieneUsuario(data.child("idUsuario").getValue().toString())
                        }
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                Log.w("conexion", "Falló la lectura de los datos.", error.toException())
            }
        })

    }

    fun obtieneUsuario(id: String) {


        fiba2.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // Clear data list

                if (dataSnapshot.exists()) {
                    // Execute read data container and populate local list as clsDato instance
                    for (data in dataSnapshot.children) {
                        val idBD = data.child("id").getValue().toString()

                        if (id == idBD) {
                            nombre_vp.text=data.child("nombre").getValue().toString()
                            telefono_vp.text=data.child("telefono").getValue().toString()
                        }
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                Log.w("conexion", "Falló la lectura de los datos.", error.toException())
            }
        })

    }

}