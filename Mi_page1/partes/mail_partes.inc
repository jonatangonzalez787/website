<style>
.mail_div {
    /* background: #252525; */
   background:  #0a2a66;
    padding: 17px;
    margin: -44px;
    border-radius: 6px;

    -webkit-box-shadow: 7px 14px 19px -10px rgba(5, 5, 5, 1);
    -moz-box-shadow: 7px 14px 19px -10px rgba(5, 5, 5, 1);
    box-shadow: 7px 14px 19px -10px rgba(5, 5, 5, 1);
}

.button2 {
    margin: auto;
    border-radius: 32px;
    background-image: 'images/lldm2.jpg';
    background-color: #202020;
    /* background: linear-gradient(220deg, #0a2a66, #0a2a66); */
    /* background: linear-gradient(180deg, #0A2AB2, #0a2a66); */
    width: 88%;
    /* border-radius: 1px; */
    /* background-color: #202020; */
    /* border: none; */
    border: 0.5px solid #4f4f4f;
    color: white;
    text-align: center;
    font-size: 15px;
    padding: 2px;
    /* width: 100px; */
    transition: all 0.5s;
    cursor: pointer;
    /* margin: 6px; */
    /* margin: 5px; */
}

.thumb {
    display: block;
    background-color: #333;
    -webkit-transition: border .2s ease-in-out;
    -o-transition: border .2s ease-in-out;
    transition: border .2s ease-in-out;
}

.middle {
    transition: .5s ease;
    opacity: 0;
    position: absolute;
    top: 15%;
    left: 50%;
    transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
}

.middle:hover {
    z-index: 1000;
    opacity: 1;
}

.contain h4 {
    font-size: 15px;
    font-weight: bold;
}

.contain:hover .image {
    opacity: 0.3;
}

.contain:hover .middle {
    opacity: 1;
}

.image {
    opacity: 1;
    display: block;
    transition: .5s ease;
    backface-visibility: hidden;
    margin: auto;
}

.ico_mp3 {
    transition: .5s ease;
    opacity: 0;
    color: red;
    position: absolute;
    top: 37%;
    left: 52%;
    transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);

}

.contain:hover .ico_mp3 {
    opacity: 1;
}

.row {
    margin-right: 0px;
    margin-left: 0px;
    padding: 15px
}

.cls_images_body_main {
    width: 100%;
    height: 400px
}

.cls_images_body {
    width: 100%;
    height: 200px
}

.cls_div_body_images {
    /* padding: 60px; */
    padding: 20px;
    color: white;
    background: #121212;
    /* BACKGROUND DE PAGINA FONDO MAIN */
    font-family: "Segoe UI", Helvetica, Arial, sans-serif;
    display: block;
}

hr {
    border-top: 1px solid #555
}

.cls_row {
    padding: 0px;
    /* background: #1c1c1c */
}

.cls_row2 {
    padding: 30px;
    background: #121212
}

.cls_row3 {
    padding: 15x
}

.cls_btn_img {
    width: 100%;
    text-align: center;
    padding-top: 15px;
    background: #252525
        /* background: linear-gradient(220deg, #0a2a66, #0a2a66); */
}

.cls_btn_img2 {
    width: 100%;
    padding: 5px;
    background: #202020
}

/* ----------------------------------MP3---------------------------------------- */
.cls_images_body_mainmp3 {
    width: 100%;
    /* height: 28%; */
    /* height: 150px */
}

/* .cls_images_body_mainmp4 {
        width: 100%;
        height: 300px;
        background-color: #355da3;
    } */

.mp3 h5 {
    color: gray;
    font-weight: bold;
    line-height: 1.5
}

.mp3 div img:hover {
    transition-duration: 1s;
    background: red;
    opacity: 0.5
}

@media (max-width:768px) {
    .cls_images_body_main {
        width: 100%;
        height: 200px
    }

    .cls_images_body {
        height: 150px
    }

}

@media (max-width:768px) {
    .mail_div {
        padding: 17px;
        margin: 0px;
    }

    .cls_div_body_images {
        /* padding-left: 0px; */
        padding: 20px;
    }

    .cls_row2 {
        padding: 15px
    }

    .cls_row3 {
        padding: 0px
    }

    .cls_background {
        background: #202020;
    }
}

.slidecontainer {
    padding-bottom: 10px;
}

@media (max-width:475px) {
    .cls_div_body_images {
        /* padding-left: 0px; */
        padding: 0px;
    }

}
</style>

<div id="formm" name="formm" class="cls_div_body_images">
    <!-- <br><br> -->
    <!-- <div style="padding: 30px;background: #202020"> -->

    <div class="cls_background cls_row">

        <div class="cls_row2">
            <h1 style="color: #ffffff">Escríbenos</h1>
            <p id="lol" style="color: #ffffff">Para una mejor atención escribenos un eMail o SMS y espera nuestra respuesta en breve.
            </p>
            <i style="display:none;color: gray">Martes 13 mayo del 2020</i>
            <hr style="color: #333"><br>

        </div>
        <!-- <br><br> -->

        <div class="row mp3" style="width: 100%;margin: auto">


            <div id="menu" style="padding:0px" class="col-xs-12 col-sm-12 col-md-12">

                <div class="contain col-xs-12 col-sm-6 col-md-6">
                <div class="cptn04">
                        <img src="https://www.dropbox.com/s/otk2q47lhnio4s1/correo8.jpg?dl=1" alt="">
                        <div class="cptn">
                            <h3>Enviar SMS</h3>
                            <p style="color: dodgerblue;line-height: 2;">Jongf Design...</p>

<!-- 
                            <a href="sms:+34695685920">Envíame un SMS</a> -->

                            <a href="sms:+50687433012" class="fa fa-long-arrow-right"></a>
                        </div>
                    </div>

                    <!-- <div class="thumb">

                        <img class="image cls_images_body_mainmp3" src="images/correo8.jpg" alt="ima">
                    </div>
                    <h4>Abdiel Joaquin</h4>
                    <h5>Dear Friend</h5>
                    <i id="ops" src="aud/drf.mp3" class="ico_mp3 fa fas fa-play"></i> -->
                </div>

                <div class="mail_div contain col-xs-12 col-sm-6 col-md-6">

                    <h3>Ingresar Datos</h3>
                    <hr style="border-top: 1px solid dodgerblue">

                    <form class="form-horizontal" method="post">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="email">Email: </label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" id="email" placeholder="Ingresar email"
                                    name="email" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">Nombre: </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="name" placeholder="Ingresar nombre"
                                    name="name" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="remember"> Recordarme</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <!-- <a href="#name"> -->
                                <button type="submit" class="btn btn-default">Enviar</button>
                                <!-- </a> -->
                            </div>
                        </div>
                    </form>
                    <?php
                    if ($_POST) {
                        include_once('partes/mail.inc');
                    }
                    ?>
                </div>
            </div>
            <!-- -----------------------------------END SOUND--------------------------------------- -->
        </div>
        <!-- <div class="row cls_btn_img">

        </div>
        fin 2do ROW -->
    </div>
    <br>
</div>
<!-- --------------------------------------------------------------------------------script--------------------- -->