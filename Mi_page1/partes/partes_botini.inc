<style>
    .roww {
        text-align: center;
        position: absolute;
        top: 1.8%;
        margin: auto;
        right: 4%;
        /* font-size: 14px; */
        font-size: 13px;
        letter-spacing: 1.5px;
        line-height: 50px
    }

    .ses1 {
        background-image: -webkit-linear-gradient(left, #3acfd5 0%, #3a4ed5 100%), -webkit-linear-gradient(left, #3acfd5 0%, #3a4ed5 100%);
        background-image: -moz-linear-gradient(left, #3acfd5 0%, #3a4ed5 100%), -moz-linear-gradient(left, #3acfd5 0%, #3a4ed5 100%);
        background-image: -o-linear-gradient(left, #3acfd5 0%, #3a4ed5 100%), -o-linear-gradient(left, #3acfd5 0%, #3a4ed5 100%);
        background-image: linear-gradient(to right, #3acfd5 0%, #3a4ed5 100%), linear-gradient(to right, #3acfd5 0%, #3a4ed5 100%);
        border-radius: 15px;
        border: 0.5px solid #0078b5;
        width: 173px;
        height: 32px;
        line-height: 32px;
        display: inline-block;
        margin-right: -16px;
    }

    .ses1 a {
        color: white
    }

    .ses1 :hover {
        opacity: 0.8;
        text-decoration: none
    }

    .ses2 :hover {
        opacity: 0.8;
        text-decoration: none
    }

    .ses2 {
        /* border-right: 0.5px solid;
    border-top: 0.5px solid;
    border-bottom: 0.5px solid; */
        width: 164px;
        height: 32px;
        color: #dadada;
        line-height: 32px;
        display: inline-block;
        border-top-right-radius: 15px;
        border-bottom-right-radius: 15px;
        background-image: -moz-linear-gradient(left, #3acfd5 0%, #3a4ed5 100%), -moz-linear-gradient(left, #3acfd5 0%, #3a4ed5 100%);
        background-image: -o-linear-gradient(left, #3acfd5 0%, #3a4ed5 100%), -o-linear-gradient(left, #3acfd5 0%, #3a4ed5 100%);
    }

    .ses2 a {
        color: white
    }

    @media screen and (max-width: 900px) {
        .roww {

            top: -560px;
        }
    }

    @media screen and (max-width: 536px) {
        .roww {
            left: 0px;
            top: -500px;
        }

        .ses1 {
            width: 101px;
        }

        .ses2 {
            width: 136px;
        }
    }
</style>

<!-- <div class="roww">

    <div class="ses2">
        <strong>
            <a href="">REGISTRARSE</a>

        </strong>

    </div>
    <div class="ses1">
        <strong>
            <a href="">INGRESAR</a>
        </strong>

    </div>

</div> -->