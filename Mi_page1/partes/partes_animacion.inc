<style>
@keyframes tipsy {
    0 {
        transform: translateX(-50%) translateY(-50%) rotate(0deg);
    }

    100% {
        transform: translateX(-50%) translateY(-50%) rotate(360deg);
    }
}

.aro {
    display: block;
    font-family: helvetica, arial, sans-serif;
    background-color: #2e2e31;
    position: inherit
}

.aro a {
    text-align: center;
    color: #fffbf1;
    /* text-shadow: 0 20px 25px #2e2e31, 0 40px 60px #2e2e31; */
    font-size: 40px;
    font-weight: bold;
    text-decoration: none;
    letter-spacing: 0px;
    /* letter-spacing: -3px; */
    margin: 0;
    position: absolute;
    /* top: 50%; */
    left: 50%;
    transform: translateX(-50%) translateY(-50%);
}

.aro a:before,
.aro a:after {
    content: '';
    padding: .9em .4em;
    position: absolute;
    left: 50%;
    width: 100%;
    top: 60%;
    display: block;
    border: 4px solid red;
    border-radius: 50%;
    transform: translateX(-50%) translateY(-50%) rotate(0deg);
    animation: 10s infinite alternate ease-in-out tipsy;
}

.aro a:before {
    border-color: #3a4ed5 #3a4ed5 rgba(0, 0, 0, 0) rgba(0, 0, 0, 0);
    /* border-color: #d9524a #d9524a rgba(0, 0, 0, 0) rgba(0, 0, 0, 0); */

    z-index: -1;
}

.aro a:after {

    border-color: rgba(0, 0, 0, 0) rgba(0, 0, 0, 0) #1972a6 #1972a6;
    /* border-color: rgba(0, 0, 0, 0) rgba(0, 0, 0, 0) #d9524a #d9524a; */
    box-shadow: 25px 25px 25px rgba(0, 0, 0, .5);
    /* box-shadow: 30px 25px 42px #242424; */

}






/* .img{
            transform:scale(1.2);
            -ms-transform:scale(1.2); // IE 9 
            -moz-transform:scale(1.2); // Firefox 
            -webkit-transform:scale(1.2); // Safari and Chrome 
            -o-transform:scale(1.2); // Opera
        } */


.aro a img {
    height: 350px;
    transform: scale(1);
    -ms-transform: scale(1);
    -moz-transform: scale(1);
    -webkit-transform: scale(1);
    -o-transform: scale(1);
    -webkit-transition: all 500ms ease-in-out;
    -moz-transition: all 500ms ease-in-out;
    -ms-transition: all 500ms ease-in-out;
    -o-transition: all 500ms ease-in-out;
}


.aro a img:hover {
    /* -webkit-transform: rotate(360deg);transform: rotate(360deg); */

    /* transform: scale(1.2);
    -ms-transform: scale(1.2);
    -moz-transform: scale(1.2);
    -webkit-transform: scale(1.2);
    -o-transform: scale(1.2); */

}

.div_grad {
    height: 90px;
    background: linear-gradient(165deg, #3a57d5, #d53a9d);
    margin: -20px
}

@media screen and (max-width: 768px) {
    .aro a img {
        height: 250px
    }
}

/* @media screen and (max-width: 536px)  */
@media screen and (max-width: 475px) {
    .aro a img {
        height: 190px
    }

    .div_grad {
        margin: -20px
    }
}
</style>

<style>
@keyframes rotate {
    to {
        transform: rotateY(0deg);
        transform: rotateY(360deg);
    }
}

/* #let2 {
        animation: 7s rotate infinite linear;
        display: block;
    }
    #let3 {
        animation: 7s rotate infinite linear;
        display: block;
    } */
</style>


<br>
<div style="display: block;">
    <h1 style="font-size: 0.8em;">Versatilidad Responsiva . . .</h1>
    <h5 style="font-weight: bold;font-weight: bold;
    letter-spacing: -0.8;font-size: 17px;">Disfruta de muchos de nuestro mejores diseños para tu gusto</h5><br><br>
</div>

<div class="aro">

    <a style="display: inline-block" target="_blank">
        <!-- Versatilidad Responsiva . . . -->
        <br>
        <hr>
        <!-- <h5 style="font-weight: bold">Disfruta de muchos de nuestro mejores diseños para tu gusto</h5><br><br> -->
        <!-- <img src="images/tbl2.png" alt=""> -->
        <img src="https://www.dropbox.com/s/gv8yj83ls30a67x/lp10.png?dl=1" alt="">
    </a>
</div>

<div style="height: 73px;"></div>
<div class="div_grad"></div>