package com.example.proyecto

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_login.*

class login : AppCompatActivity() {
    // Crea instancia de la base de datos para ser utilizada
    // Create local variables but not initialized
    lateinit var fiba: DatabaseReference
    lateinit var lstdatos: MutableList<clsUsuario>
    lateinit var token: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        fiba = FirebaseDatabase.getInstance().getReference("usuario")
        lstdatos = mutableListOf()
        token = ""


        btnIniciar.setOnClickListener {
            obtieneUsuario( txtCorreo.text.toString().trim { it <= ' ' }, txtContra.text.toString().trim { it <= ' ' })
            finish()

        }
        btnCrearCuenta.setOnClickListener {
            var intent2: Intent = Intent(this, registrar::class.java)
            startActivity(intent2)
            finish()
        }

    }
    fun obtieneUsuario(correo:String, contra:String){


        fiba.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // Clear data list

                if(dataSnapshot.exists()) {
                    // Execute read data container and populate local list as clsDato instance
                    var verifica: Boolean=false
                    for (data in dataSnapshot.children) {
                        val correoBD=data.child("correo").getValue().toString()
                        val contraBD=data.child("contra").getValue().toString()
                        val idBD=data.child("id").getValue().toString()

                        if(correoBD==correo && contraBD==contra) {
                             verifica=true
                            clsRegistrar(this@login).execute(
                                txtCorreo.text.toString().trim { it <= ' ' },
                                txtContra.text.toString().trim { it <= ' ' },
                                idBD.toString()
                            )

                            var intent2: Intent = Intent(this@login, MainActivity::class.java)
                            startActivity(intent2)
                        }
                    }
                    if(verifica==false)
                        alerta("El usuario no esta registrado por favor verifique que el usuario y contraseña esten escritos correctamente","¡Error!")
                }
            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                Log.w("conexion", "Falló la lectura de los datos.", error.toException())
            }
        })

    }
    fun alerta(mensaje:String, titulo:String){
        val dialogBuilder = AlertDialog.Builder(this)

        // set message of alert dialog
        dialogBuilder.setMessage(mensaje)
            // if the dialog is cancelable
            .setCancelable(false)

            // negative button text and action
            .setNegativeButton("Aceptar", DialogInterface.OnClickListener {
                    dialog, id -> dialog.cancel()
            })

        // create dialog box
        val alert = dialogBuilder.create()
        // set title for alert dialog box
        alert.setTitle(titulo)
        // show alert dialog
        alert.show()
    }

    //Crea una clase local para registrar datos de usuario en API remota y almacenamiento local
    internal class clsRegistrar(private val activity: Activity) :
        AsyncTask<String?, Void?, Array<String?>>() {
        override fun doInBackground(vararg params: String?): Array<String?>? {
            try {

                //Registra los datos en el local storage
                val prefe = activity.getSharedPreferences("appData", Context.MODE_PRIVATE)

                //Crea un editor para escribir los datos de la app
                val editor = prefe.edit()

                // Configura los campos del editor con los nuevos valores
                editor.putString("correo", params[0])
                editor.putString("contra", params[1])
                editor.putString("id", params[2])

                // Escribe los datos
                editor.commit()
            } catch (ex: Exception) {
                val trace = ex.stackTrace
                var i = 0
                while (i < trace.size) {
                    Log.d("Persistencia", trace[i].toString())
                    i++
                }
            }
            return arrayOfNulls(0)
        } // fin de la función en Background
    } // fin de la clase local
}