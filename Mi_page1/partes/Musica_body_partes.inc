<style>
    .button2 {
        margin: auto;
        border-radius: 32px;
        background-image: 'https://www.dropbox.com/s/z4c6gwho9ph7len/lldm2.jpg?dl=1';
        background-color: #202020;
        /* background: linear-gradient(220deg, #0a2a66, #0a2a66); */
        /* background: linear-gradient(180deg, #0A2AB2, #0a2a66); */
        width: 88%;
        /* border-radius: 1px; */
        /* background-color: #202020; */
        /* border: none; */
        border: 0.5px solid #4f4f4f;
        color: white;
        text-align: center;
        font-size: 15px;
        padding: 2px;
        /* width: 100px; */
        transition: all 0.5s;
        cursor: pointer;
        /* margin: 6px; */
        /* margin: 5px; */
    }

    .thumb {
        display: block;
        background-color: #333;
        -webkit-transition: border .2s ease-in-out;
        -o-transition: border .2s ease-in-out;
        transition: border .2s ease-in-out;
    }

    .middle {
        transition: .5s ease;
        opacity: 0;
        position: absolute;
        top: 15%;
        left: 50%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
    }

    .middle:hover {
        z-index: 1000;
        opacity: 1;
    }

    .contain h4 {
        font-size: 15px;
        font-weight: bold;
    }

    .contain:hover .image {
        opacity: 0.3;
    }

    .contain:hover .middle {
        opacity: 1;
    }

    .image {
        opacity: 1;
        display: block;
        transition: .5s ease;
        backface-visibility: hidden;
        margin: auto;
    }

    .ico_mp3 {
        transition: .5s ease;
        opacity: 0;
        color: red;
        position: absolute;
        /* top: 30%; */
        top: 50%;
        left: 52%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);

    }

    .contain {
        padding: 6px
    }

    .contain:hover .ico_mp3 {
        opacity: 1;
    }

    .row {
        margin-right: 0px;
        margin-left: 0px;
        padding: 15px
    }

    .cls_images_body_main {
        width: 100%;
        height: 400px
    }

    .cls_images_body {
        width: 100%;
        height: 200px
    }

    .cls_div_body_images {
        /* padding: 60px; */
        padding: 20px;
        color: white;
        background: #121212;
        /* BACKGROUND DE PAGINA FONDO MAIN */
        font-family: "Segoe UI", Helvetica, Arial, sans-serif;
        display: block;
    }

    hr {
        border-top: 1px solid #555
    }

    .cls_row {
        padding: 0px;
        /* background: #1c1c1c */
    }

    .cls_row2 {
        padding: 30px;
        background: #121212
    }

    .cls_row3 {
        padding: 15x
    }

    .cls_btn_img {
        width: 100%;
        text-align: center;
        padding-top: 15px;
        background: #252525
            /* background: linear-gradient(220deg, #0a2a66, #0a2a66); */
    }

    .cls_btn_img2 {
        width: 100%;
        padding: 5px;
        background: #202020
    }

    /* ----------------------------------MP3---------------------------------------- */
    .cls_images_body_mainmp3 {
        width: 100%;
        /* height: 28%; */
        /* height: 150px */
    }

    /* .cls_images_body_mainmp4 {
        width: 100%;
        height: 300px;
        background-color: #355da3;
    } */

    .mp3 h5 {
        color: gray;
        font-weight: bold;
        line-height: 1.5
    }

    .mp3 div img:hover {
        transition-duration: 1s;
        background: red;
        opacity: 0.5
    }

    @media (max-width:768px) {
        .cls_images_body_main {
            width: 100%;
            height: 200px
        }

        .cls_images_body {
            height: 150px
        }

    }

    @media (max-width:768px) {

        .cls_div_body_images {
            /* padding-left: 0px; */
            padding: 20px;
        }

        .cls_row2 {
            padding: 15px
        }

        .cls_row3 {
            padding: 0px
        }

        .cls_background {
            background: #202020;
        }
    }

    .slidecontainer {
        padding-bottom: 10px;
    }

    @media (max-width:475px) {
        .cls_div_body_images {
            /* padding-left: 0px; */
            padding: 0px;
        }

    }
</style>

<div class="cls_div_body_images">



<!-- <a href="https://api.whatsapp.com/send?phone=50687433012&text=hola,%20¿qué%20tal%20estás?">APIWATHSAAP-Mensaje</a>
<a href="sms:+50687433012?body=Me%20interesa%20el%20auto%20que%20estás%20vendiendo">msg SMS->Me interesa, go!</a>

<a href="tel:50687433012">Call</a><br>
a.me/50687433012?text=Mensaje"><p>Envíame un wathsaap</p>

<a href="sms:+50687433012?body=Mensaje">Texto a mostrar con el enlace</a><br>
<a href="sms:+50687433012">Envíame un SMS</a> -->

    <!-- <br><br> -->
    <!-- <div style="padding: 30px;background: #202020"> -->

    <div class="cls_background cls_row">

        <div id="musica" class="cls_row2">
            <h1 style="color: #ffffff">Archivos de Música</h1>
            <p class="cls_formato_p">Archivos mp3 que busqué en internet y aquí muestro algunos
                que escogí.</p>
            <i style="display:none;color: gray">Martes 13 mayo del 2020</i>
            <hr style="color: #333">

        </div>

        <!-- <br><br> -->

        <div class="body_main row mp3" style="width: 100%;margin: auto;height: 485px;overflow-y: scroll;">


            <div id="menu" style="padding:0px" class="col-xs-12 col-sm-12 col-md-12">

                <div class="contain col-xs-4 col-sm-2 col-md-2">
                    <div class="thumb">
                        <!-- <i id="ops" src="https://www.dropbox.com/s/1uex3ohl78icefi/drf.mp3?dl=1" class="ico_mp3 fa fas fa-play"></i> -->
                        <img class="image cls_images_body_mainmp3" src="https://www.dropbox.com/s/clwlgnsjjbk0u5n/aud8.jpg?dl=1" alt="ima">

                    </div>
                    <h4>Abdiel Joaquin</h4>
                    <h5>Dear Friend</h5>
                    <i id="ops" src="https://www.dropbox.com/s/x5v1sd04xu99lni/drf.mp3?dl=1" class="ico_mp3 fa fas fa-play"></i>
                </div>

                <div class="contain col-xs-4 col-sm-2 col-md-2">
                    <div class="thumb">
                        <img class="image cls_images_body_mainmp3" src="https://www.dropbox.com/s/clwlgnsjjbk0u5n/aud8.jpg?dl=1" alt="ima">
                    </div>
                    <h4>Abdiel Joaquin</h4>
                    <h5>Al Altar</h5>
                    <i id="ops" src="https://www.dropbox.com/s/b5ktpvpl910oe4x/alt.mp3?dl=1" class="ico_mp3 fa fas fa-play"></i>
                </div>
                <div class="contain col-xs-4 col-sm-2 col-md-2">
                    <div class="thumb">
                        <img class="image cls_images_body_mainmp3" src="https://www.dropbox.com/s/clwlgnsjjbk0u5n/aud8.jpg?dl=1" alt="ima">
                    </div>
                    <h4>Abdiel Joaquin</h4>
                    <h5>Y si resucito</h5>
                    <i id="ops" src="https://www.dropbox.com/s/7nqxjyr024bftjm/ysi.mp3?dl=1" class="ico_mp3 fa fas fa-play"></i>
                </div>

                <div class="contain col-xs-4 col-sm-2 col-md-2">
                    <div class="thumb">
                        <img class="image cls_images_body_mainmp3" src="https://www.dropbox.com/s/clwlgnsjjbk0u5n/aud8.jpg?dl=1" alt="ima">
                    </div>
                    <h4>Abdiel Joaquin</h4>
                    <h5>Canción Misionero</h5>
                    <i id="ops" src="https://www.dropbox.com/s/vj28xm5dw083yh4/mis.mp3?dl=1" class="ico_mp3 fa fas fa-play"></i>
                </div>
                <div class="contain col-xs-4 col-sm-2 col-md-2">
                    <div class="thumb">
                        <img class="image cls_images_body_mainmp3" src="https://www.dropbox.com/s/clwlgnsjjbk0u5n/aud8.jpg?dl=1" alt="ima">
                    </div>
                    <h4>Abdiel Joaquin</h4>
                    <h5>Todo el Mundo</h5>
                    <i id="ops" src="https://www.dropbox.com/s/u1f6bok8onm8y7q/tod.mp3?dl=1" class="ico_mp3 fa fas fa-play"></i>
                </div>

                <div class="contain col-xs-4 col-sm-2 col-md-2">
                  
                    <div class="thumb">
                        <img class="image cls_images_body_mainmp3" src="https://www.dropbox.com/s/clwlgnsjjbk0u5n/aud8.jpg?dl=1" alt="ima">

                    </div>
                    <h4>Abdiel Joaquin</h4>
                    <h5>Lloran los corazones</h5>
                    <i id="ops" src="https://www.dropbox.com/s/5fnscgvcgrv12x6/llo.mp3?dl=1" class="ico_mp3 fa fas fa-play"></i>
                </div>

            </div>

            <!-- -----------------------------------------------------------ROW 2 mp3----------------------------------- -->

            <div id="menu" style="padding:0px" class="col-xs-12 col-sm-12 col-md-12">

                <div class="contain col-xs-4 col-sm-2 col-md-2">
                    <div class="thumb">
                        <img class="image cls_images_body_mainmp3" src="https://www.dropbox.com/s/clwlgnsjjbk0u5n/aud8.jpg?dl=1" alt="ima">
                    </div>
                    <h4>Carreón y Helem</h4>
                    <h5>En su corazón</h5>
                    <i id="ops" src="https://www.dropbox.com/s/aszq4642ovltupg/en%20su%20corazon.mp3?dl=1" class="ico_mp3 fa fas fa-play"></i>
                </div>

                <div class="contain col-xs-4 col-sm-2 col-md-2">
                    <div class="thumb">
                        <img class="image cls_images_body_mainmp3" src="https://www.dropbox.com/s/clwlgnsjjbk0u5n/aud8.jpg?dl=1" alt="ima">
                    </div>
                    <h4>Orfeón LLDM</h4>
                    <h5>La luz de la vida</h5>
                    <i id="ops" src="https://www.dropbox.com/s/ybzpcl3dn5lxxek/La%20Luz%20De%20La%20Vida%20%20Coros%20LLDMfrancia.mp3?dl=1" class="ico_mp3 fa fas fa-play"></i>
                </div>
                <div class="contain col-xs-4 col-sm-2 col-md-2">
                    <div class="thumb">
                        <img class="image cls_images_body_mainmp3" src="https://www.dropbox.com/s/clwlgnsjjbk0u5n/aud8.jpg?dl=1" alt="ima">
                    </div>
                    <h4>Andrés F.Canales</h4>
                    <h5>No me puedo imaginar</h5>
                    <i id="ops" src="https://www.dropbox.com/s/0c56bi0a5qmnwdg/LLDM-%20NO%20ME%20PUEDO%20IMAGINARandres%20canales.mp3?dl=1" class="ico_mp3 fa fas fa-play"></i>
                </div>

                <div class="contain col-xs-4 col-sm-2 col-md-2">
                    <div class="thumb">
                        <img class="image cls_images_body_mainmp3" src="https://www.dropbox.com/s/clwlgnsjjbk0u5n/aud8.jpg?dl=1" alt="ima">
                    </div>
                    <h4>Hnos Medina</h4>
                    <h5>Siempre te amaré</h5>
                    <i id="ops" src="https://www.dropbox.com/s/2yannr7i1z94e5u/Hermanos%20Medina%20Siempre%20te%20amare.mp3?dl=1" class="ico_mp3 fa fas fa-play"></i>
                </div>
                <div class="contain col-xs-4 col-sm-2 col-md-2">
                    <div class="thumb">
                        <img class="image cls_images_body_mainmp3" src="https://www.dropbox.com/s/clwlgnsjjbk0u5n/aud8.jpg?dl=1" alt="ima">
                    </div>
                    <h4>Shaloom</h4>
                    <h5>Mi Señor en tu amor</h5>
                    <i id="ops" src="https://www.dropbox.com/s/ufh43q1v8mivhgd/mise%C3%B1.mp3?dl=1" class="ico_mp3 fa fas fa-play"></i>
                </div>

                <div class="contain col-xs-4 col-sm-2 col-md-2">
                    <div class="thumb">
                        <img class="image cls_images_body_mainmp3" src="https://www.dropbox.com/s/clwlgnsjjbk0u5n/aud8.jpg?dl=1" alt="ima">
                    </div>
                    <h4>Rondalla Berea</h4>
                    <h5>A mi Padre</h5>
                    <i id="ops" src="https://www.dropbox.com/s/msq7vq7z3f8gyvf/amip.mp3?dl=1" class="ico_mp3 fa fas fa-play"></i>
                </div>

            </div>

            <!-- -----------------------------------------------------------ROW 2 mp3----------------------------------- -->

            <div id="menu" style="padding:0px" class="col-xs-12 col-sm-12 col-md-12">

                <div class="contain col-xs-4 col-sm-2 col-md-2">
                    <div class="thumb">
                        <img class="image cls_images_body_mainmp3" src="https://www.dropbox.com/s/clwlgnsjjbk0u5n/aud8.jpg?dl=1" alt="ima">
                    </div>
                    <h4>Cuando la tarde</h4>
                    <h5>El Samaritano</h5>
                    <i id="ops" src="https://www.dropbox.com/s/yesm5jv8k06uuih/CUANDO%20LA%20TARDE%20LLEGA%20%28El%20Samaritano%29%20LLDM.mp3?dl=1" class="ico_mp3 fa fas fa-play"></i>
                </div>

                <div class="contain col-xs-4 col-sm-2 col-md-2">
                    <div class="thumb">
                        <img class="image cls_images_body_mainmp3" src="https://www.dropbox.com/s/clwlgnsjjbk0u5n/aud8.jpg?dl=1" alt="ima">
                    </div>
                    <h4>Voces Unidas</h4>
                    <h5>Haciendo Historia</h5>
                    <i id="ops" src="https://www.dropbox.com/s/dx1a7u9vsst0lq1/Juntos%20Haciendo%20Historia.mp3?dl=1" class="ico_mp3 fa fas fa-play"></i>
                </div>
                <div class="contain col-xs-4 col-sm-2 col-md-2">
                    <div class="thumb">
                        <img class="image cls_images_body_mainmp3" src="https://www.dropbox.com/s/clwlgnsjjbk0u5n/aud8.jpg?dl=1" alt="ima">
                    </div>
                    <h4>Voces Unidas</h4>
                    <h5>Vamos con seguridad</h5>
                    <i id="ops" src="https://www.dropbox.com/s/yodfzl39suagvxj/Vamos%20con%20seguridad%20%28VIDEO%20OFFICIAL%29%20LLDM%20%20Voces%20Unidas.mp3?dl=1" class="ico_mp3 fa fas fa-play"></i>
                </div>

                <div class="contain col-xs-4 col-sm-2 col-md-2">
                    <div class="thumb">
                        <img class="image cls_images_body_mainmp3" src="https://www.dropbox.com/s/clwlgnsjjbk0u5n/aud8.jpg?dl=1" alt="ima">
                    </div>
                    <h4>David FG</h4>
                    <h5>Por gracia me tocó</h5>
                    <i id="ops" src="https://www.dropbox.com/s/yjoy43ibgc7f3sm/Por%20Gracia%20Me%20Toc%C3%B3%20%20LLDM_DAVIDFG.mp3?dl=1" class="ico_mp3 fa fas fa-play"></i>
                </div>
                <div class="contain col-xs-4 col-sm-2 col-md-2">
                    <div class="thumb">
                        <img class="image cls_images_body_mainmp3" src="https://www.dropbox.com/s/clwlgnsjjbk0u5n/aud8.jpg?dl=1" alt="ima">
                    </div>
                    <h4>Nicol</h4>
                    <h5>No me rendiré</h5>
                    <i id="ops" src="https://www.dropbox.com/s/x546zrmtde8h85y/Nicole%20-%20No%20me%20rendire%20%28mp3cut.net%29.mp3?dl=1" class="ico_mp3 fa fas fa-play"></i>
                </div>

                <div class="contain col-xs-4 col-sm-2 col-md-2">
                    <div class="thumb">
                        <img class="image cls_images_body_mainmp3" src="https://www.dropbox.com/s/clwlgnsjjbk0u5n/aud8.jpg?dl=1" alt="ima">
                    </div>
                    <h4>Nicol</h4>
                    <h5>Lo que mi corazón siente</h5>
                    <i id="ops" src="https://www.dropbox.com/s/owgrl3yytv7e7n4/Nicole%20-%20Lo%20Que%20Mi%20Coraz%C3%B3n%20Siente.mp3?dl=1" class="ico_mp3 fa fas fa-play"></i>
                </div>

            </div>
            <!-- ----------------------------------------------------------------------------------------- -->
            <!-- -----------------------------------------------------------ROW 2 mp3----------------------------------- -->

            <div id="menu" style="padding:0px" class="col-xs-12 col-sm-12 col-md-12">

                <div class="contain col-xs-4 col-sm-2 col-md-2">
                    <div class="thumb">
                        <img class="image cls_images_body_mainmp3" src="https://www.dropbox.com/s/clwlgnsjjbk0u5n/aud8.jpg?dl=1" alt="ima">
                    </div>
                    <h4>Niños LLDM</h4>
                    <h5>Que lindo es</h5>
                    <i id="ops" src="https://www.dropbox.com/s/isb5h2mnihyq7c5/LLDM-%20Que%20Lindo%20Es.mp3?dl=1" class="ico_mp3 fa fas fa-play"></i>
                </div>

                <div class="contain col-xs-4 col-sm-2 col-md-2">
                    <div class="thumb">
                        <img class="image cls_images_body_mainmp3" src="https://www.dropbox.com/s/clwlgnsjjbk0u5n/aud8.jpg?dl=1" alt="ima">
                    </div>
                    <h4>Orfeón LLDM</h4>
                    <h5>En su corazón</h5>
                    <i id="ops" src="https://www.dropbox.com/s/2jhbbp5gw66jb87/en%20su%20corazon%20coro.mp3?dl=1" class="ico_mp3 fa fas fa-play"></i>
                </div>

                <div class="contain col-xs-4 col-sm-2 col-md-2">
                    <div class="thumb">
                        <img class="image cls_images_body_mainmp3" src="https://www.dropbox.com/s/clwlgnsjjbk0u5n/aud8.jpg?dl=1" alt="ima">
                    </div>
                    <h4>Orfeón Universal</h4>
                    <h5>La fiesta más grande</h5>
                    <i id="ops" src="https://www.dropbox.com/s/laukevhkucsh15x/LLDM%20-%20La%20Fiesta%20Mas%20Grande%20%28Coro%20Universal%29.mp3?dl=1" class="ico_mp3 fa fas fa-play"></i>
                </div>
                <div class="contain col-xs-4 col-sm-2 col-md-2">
                    <div class="thumb">
                        <img class="image cls_images_body_mainmp3" src="https://www.dropbox.com/s/clwlgnsjjbk0u5n/aud8.jpg?dl=1" alt="ima">
                    </div>
                    <h4>Orfeón LLDM</h4>
                    <h5>León de Judá</h5>
                    <i id="ops" src="https://www.dropbox.com/s/v3myouj6rwgopcj/leon_jud%C3%A1.mp3?dl=1" class="ico_mp3 fa fas fa-play"></i>
                </div>

                <div class="contain col-xs-4 col-sm-2 col-md-2">
                    <div class="thumb">
                        <img class="image cls_images_body_mainmp3" src="https://www.dropbox.com/s/clwlgnsjjbk0u5n/aud8.jpg?dl=1" alt="ima">
                    </div>
                    <h4>María y Marta</h4>
                    <h5>No soy nada</h5>
                    <i id="ops" src="https://www.dropbox.com/s/vdsuhz9kcrxxqy1/nos.mp3?dl=1" class="ico_mp3 fa fas fa-play"></i>
                </div>
                <div class="contain col-xs-4 col-sm-2 col-md-2">
                    <div class="thumb">
                        <img class="image cls_images_body_mainmp3" src="https://www.dropbox.com/s/clwlgnsjjbk0u5n/aud8.jpg?dl=1" alt="ima">
                    </div>
                    <h4>Orfeón M.Guadalajara</h4>
                    <h5>Honorable</h5>
                    <i id="ops" src="https://www.dropbox.com/s/0uvw8u88l2tkoiz/HONORABLE%20-%20Coro%20Metropolitano%20de%20Guadalajara.mp3?dl=1" class="ico_mp3 fa fas fa-play"></i>
                </div>



            </div>
            <!-- ----------------------------------------------------------------------------------------- -->

            <!-- -------------------------------------SOUND----------------------------------------- -->

            <p style="color: red" id="demo"></p>
            <audio id="audio" preload="auto">
                <source src="">
                </source>
            </audio>
            <!-- -----------------------------------END SOUND--------------------------------------- -->

        </div>
        <div class="row cls_btn_img">

            <div>
                <div onclick="myFunction1()" class="button2">
                    <i id="mut" onclick="func_volup()" class="fa fas fa-volume-up fa-1x"></i>
                    <i style="display: none" id="nomut" onclick="func_mute()" class="fa fas fa-volume-mute fa-1x"></i>
                    <i onclick="func_atrazar()" class="fa fas fa-reply fa-1x"></i>
                    <!-- <i onclick="func_atrazar()" class="fa fas fa-play fa-1x"></i> -->
                    <i id="play" class="fa fas fa-play fa-1x"></i>
                    <i style="display:none" id="pause" class="audio_control fa fas fa-pause fa-1x"></i>
                    <i onclick="func_adelantar()" class="fa fas fa-share fa-1x"></i>
                    <i onclick="func_stop()" class="fa fas fa-stop fa-1x"></i>
                    <span></span></div>

                <br>

                <div class="slidecontainer">
                    <input type="range" min="0" max="1" value="0.5" step="0.1" class="slider" id="mislider">
                </div>

                <span style="display: none" id="valor"></span>

                <span style="color:gray" id="currentTime">00:00:00</span>
                <canvas style="border-radius: 50px;
                            background: #333;
                            width: 80%;
                            height: 5px;" id="timer" width="300" height="7"></canvas> <span style="color: gray" id="duration">00:00:00</span>

            </div>
        </div>


        <p style="color: red" id="demo"></p>
        <audio id="audio" preload="auto">
            <source src="">
            </source>
        </audio>




        <!-- </div> -->
        <!-- fin 2do ROW -->

    </div>
    <br>

</div>
<!-- --------------------------------------------------------------------------------script--------------------- -->

<style>
    .slidecontainer {
        /* width: 100px; */
        width: 180px;
        margin: auto;
    }

    .slider {
        -webkit-appearance: none;
        width: 60%;
        height: 3px;
        background: #d3d3d3;
        outline: none;
        opacity: 0.7;
        -webkit-transition: .2s;
        transition: opacity .2s;
    }

    .slider:hover {
        opacity: 1;
    }

    .slider::-webkit-slider-thumb {
        -webkit-appearance: none;
        appearance: none;
        width: 12px;
        height: 12px;
        background: black;
        cursor: auto;
    }

    .slider::-moz-range-thumb {
        width: 25px;
        height: 25px;
        background: #4CAF50;
        cursor: pointer;
    }

    @media screen and (max-width: 500px) {

        audio,
        canvas,
        progress,
        video {
            display: block;
            margin: auto;
            width: 80%;
        }
    }
</style>

<style>
    .fa {
        /* padding-bottom: 20px;
        padding-left: 20px;
        padding-right: 20px;
        padding-top: 14px; */
        /* color: black */
    }

    .fa-1x {
        /* padding: 14px;
        font-size: 1.1em */
        color: #dadada;
        /* border: 0.5px solid #dadada; */
        border-radius: 50%;
        /* background: #dadada; */
        padding: 8px;
        margin-right: 10px;
        margin-left: 10px;
        font-size: 1.1em
            /* color: black */
        ;
    }

    .span {
        color: black
    }
</style>


<script>
    $('#menu i').on('click', function() {

        $('i.activo').removeClass('activo');
        $(this).addClass('activo');
        var $val = $('#ops.activo').attr('src');
        $('#audio').attr('src', $val); +
        document.getElementById("audio").play();
        $('#play').hide();
        $('#pause').show();

    });

    function func_stop() {

        audio.pause();
        audio.currentTime = 0;
        $('#play').show();
        $('#pause').hide();

    }

    function func_adelantar() {
        audio.currentTime = audio.currentTime + 10;
    };

    function func_atrazar() {
        audio.currentTime = audio.currentTime - 10;
        audio.play();
        // prompt("Introduzca su nombre:", audio.currentTime);
    };

    function func_volup() {
        document.getElementById('nomut').style.display = 'inline-block';
        document.getElementById('mut').style.display = 'none';
        document.getElementById("audio").volume = 0;

    };

    function func_mute() {
        var $vol = $("#mislider").attr("value");
        document.getElementById('mut').style.display = 'inline-block';
        document.getElementById('nomut').style.display = 'none';
        document.getElementById("audio").volume = 0.5;

    };
    //  --------------------------------------------------------------------------------------------------   
    var reproductor = document.getElementById("audio");
    reproductor.volume = 0.5;

    var barra = document.getElementById("mislider");
    barra.addEventListener("change", function(ev) {
        var resultado = document.getElementById("valor");
        resultado.innerHTML = ev.currentTarget.value;
        reproductor.volume = ev.currentTarget.value;

    }, true);

    document.getElementById("valor").innerHTML = document.getElementById("mislider").value;
</script>



<script>
    var audio = false;
    var canvas = false;
    var context = false;
    var paused = false;

    var updateCanvas = function(percent) {
        //Obtenemos la anchura del canvas
        var w = canvas.width * percent / 100;

        //Dibujamos un rectángulo relleno de blanco (limpiamos la imagen)
        context.fillStyle = "#333";
        context.fillRect(0, 0, canvas.width, canvas.height);

        //Dibujamos un rectángulo de relleno gris 

        context.fillStyle = "red";
        context.fillRect(0, 0, w, canvas.height);


        //Lo envolvemos con una caja de color naranja
        context.strokeStyle = "transparent";
        context.strokeRect(0, 0, canvas.width, canvas.height);
    }

    var canvasClicked = function(event) {
        //Hacemos una regla de tres para colocar el timer del reproductor 
        //en un tiempo directamente proporcional a la zona que hemos pinchado
        audio.currentTime = audio.duration * event.offsetX / canvas.width;
    }

    //Convierte segundos (double) a hh:mm:ss
    var toHMS = function(it) {
        if (isNaN(it)) {
            return "00:00:00";
        }

        var hours = parseInt(it / 3600);
        var minutes = parseInt((it % 3600) / 60);
        var seconds = parseInt((it % 3600) % 60);

        return ((hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" +
            (
                seconds <
                10 ? "0" + seconds : seconds));
    }

    var pause = function() {
        //Pausamos la reproducción
        audio.pause();
        paused = true;

        //Escondemos el pause y mostramos el play
        $('#pause').hide();
        $('#play').show();
    }

    var play = function() {
        //Iniciamos la reproducción
        audio.play();
        paused = false;

        //Escondemos el play y mostramos el pause
        $('#play').hide();
        $('#pause').show();
    }

    var updateTimes = function() {
        //Actualizamos el display de tiempo transcurrido
        $('#currentTime').html(toHMS(audio.currentTime));

        //La línea se rellenará con el porcentaje de tiempo transcurrido
        updateCanvas(parseInt(audio.currentTime * 100 / audio.duration));
    }
    var init = function() {
        //Inicializamos el player
        audio = document.getElementById('audio');

        //Asociamos eventos a los botones de play y pause
        $('#play').click(play);
        $('#pause').click(pause);
        //Definimos la duración de la pista de audio, así como el valor del volumen (de 0 a 100)
        $('#duration').html(toHMS(audio.duration));
        setInterval(function() {
            updateTimes();
        }, 100);

        //Inicializamos el canvas
        canvas = document.getElementById('timer');
        $('#timer').click(canvasClicked);
        context = canvas.getContext('2d');
    };

    $(document).ready(function() {
        $('.audio_control').click(function(event) {
            event
                .preventDefault(); //Evitamos que se mueva la ventana al hacer clic en los enlaces
        });

        $('#audio').bind('loadedmetadata',
            init); //Lanzamos la función init cuando se hayan cargado los metadatos

    });
</script>