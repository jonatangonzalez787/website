<style>
  .social_b {
    /* position: absolute;
    top: 3%;
    right: 2%; */

    position: absolute;
    top: 5.5%;
    right: 2%;
  }

  .social_b a {
    display: block;
    padding: 5px;
  }

  @media screen and (max-width: 536px) {
    .social_b {
      display: none
    }
  }
</style>
<div class="social_b">
  <a href="" target="_blank">
    <img src="//www.hostinger.es/assets/images/footer/socials/linked-in-853d699c9d.svg" alt="Linked-in logo"></a>
  <a href="" target="_blank">
    <img src="//www.hostinger.es/assets/images/footer/socials/instagram-4937c66486.svg" alt="Instagram logo"></a>
  <a href="https://www.facebook.com/jonatan.g.f" rel="noopener noreferrer nofollow" target="_blank">
    <img src="//www.hostinger.es/assets/images/footer/socials/facebook-fd035ec457.svg" alt="Facebook logo"></a>
  <a href="" target="_blank">
    <img src="//www.hostinger.es/assets/images/footer/socials/twitter-ac4c771131.svg" alt="Twitter logo"></a>
</div>