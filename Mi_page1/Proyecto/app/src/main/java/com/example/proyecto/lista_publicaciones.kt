package com.example.proyecto

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.AdapterView
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_lista_publicaciones.*

class lista_publicaciones:AppCompatActivity() {
    // Create local variables but not initialized
    lateinit var fiba: DatabaseReference
    lateinit var lstdatos: MutableList<clsPublicacion>
    lateinit var token: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lista_publicaciones)

        // Instance variables
        fiba = FirebaseDatabase.getInstance().getReference("publicaciones")
        lstdatos = mutableListOf()
        token = ""

        lstPublicaciones.onItemClickListener =AdapterView.OnItemClickListener { parent, view, position, id ->
               token = (lstPublicaciones.getItemAtPosition(position) as clsPublicacion).id

            val laotra = Intent(this, visualiza_publicacion::class.java)
            laotra.putExtra("id", token)
            startActivity(laotra)
        }

        // Read from database when data changed
        fiba.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                var provincia = intent.extras!!.get("provincia")!!.toString()
                var canton = intent.extras!!.get("canton")!!.toString()
                var categoria = intent.extras!!.get("categoria")!!.toString()
                var subCategoria = intent.extras!!.get("subcategoria")!!.toString()
                var validador=0
                // Clear data list
                if(lstdatos.count() > 0){
                    lstdatos.clear()
                }
                if(dataSnapshot.exists()) {
                    // Execute read data container and populate local list as clsDato instance
                    for (data in dataSnapshot.children) {
                        val valor = data.getValue(clsPublicacion::class.java)!!

                        if(valor.provincia==provincia||provincia=="Todos"){
                          validador++
                        }
                        if(valor.canton==canton||canton=="Todos"){
                            validador++
                        }
                        if(valor.categoria==categoria||categoria=="Todos"){
                            validador++
                        }
                        if(valor.subcategoria==subCategoria||subCategoria=="Todos"){
                            validador++
                        }
                        if(validador==4) {
                            lstdatos.add(valor)
                        }
                        validador=0
                    }

                    // Create adapter as view_personas collection
                    val personaAdapter = lstPublicacionAdapter(applicationContext, R.layout.view_publicaciones, lstdatos)

                    // Refresh listView data
                    lstPublicaciones.adapter = personaAdapter
                }
            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                Log.w("lista_publicaciones", "Falló la lectura de los datos.", error.toException())
            }
        })

    }

}