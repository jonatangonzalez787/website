<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="estilos/styles.css">
<link rel="stylesheet" href="estilos/footer.css">
<link rel="stylesheet" href="estilos/image_header.css">
<link rel="stylesheet" href="estilos/span-icon_partes.css">
<link rel="stylesheet" href="estilos/box_images.css">
<link rel="stylesheet" href="estilos/img_main_mail.css">
<!-- <link rel="stylesheet" href="estilos/bootstrap.css"> -->
<!-- <link href="estilos/fontawesome/css/all.css" rel="stylesheet">  -->
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet'>

<link href='https://fonts.googleapis.com/css?family=Jost' rel='stylesheet'>

<link href='https://fonts.googleapis.com/css?family=Anton' rel='stylesheet'>

<link href='https://fonts.googleapis.com/css?family=Cabin' rel='stylesheet'><!-- usar menu -->
<link href='https://fonts.googleapis.com/css?family=Varela' rel='stylesheet'><!-- titulo abajo princ h4 -->
<link href='https://fonts.googleapis.com/css?family=Righteous' rel='stylesheet'><!-- titulo bonito grueso -->
<link href='https://fonts.googleapis.com/css?family=Kanit' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Prompt' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Ramabhadra' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Sen' rel='stylesheet'>
