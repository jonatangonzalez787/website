package com.example.proyecto

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_registrar_publicacion.*
import java.util.ArrayList

class registrar_publicacion: AppCompatActivity() {
    lateinit var cmbProvincia: Spinner
    lateinit var cmbCanton: Spinner

    lateinit var cmbCategoria: Spinner
    lateinit var cmbSubCategoria: Spinner

    lateinit var resCategoria: String
    lateinit var resSubCategoria: String
    lateinit var resProvincia: String
    lateinit var resCanton: String

    // Crea instancia de la base de datos para ser utilizada
    lateinit var fiba: DatabaseReference
    lateinit var lstdatos: MutableList<clsPublicacion>
    lateinit var token: String



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registrar_publicacion)

        fiba = FirebaseDatabase.getInstance().getReference("publicaciones")
        lstdatos = mutableListOf()
        token = ""

        resCategoria=""
        resSubCategoria=""
        resProvincia=""
        resCanton=""

        /*--------------------------------------codigo del comboBox provincia y canton--------------------------------------------*/
        var provincia:String="San José"
        val opcionesProvincia= arrayOf(
            "San José",
            "Alajuela",
            "Cartago",
            "Heredia",
            "Guanacaste",
            "Puntarenas",
            "Limón")

        cmbProvincia = findViewById(R.id.cmbProvincia) as Spinner
        cmbCanton = findViewById(R.id.cmbCanton) as Spinner


        var adapter= ArrayAdapter(this,android.R.layout.simple_list_item_1,opcionesProvincia)
        cmbProvincia.adapter=adapter

        //LISTENER
        cmbProvincia.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                val opcionesCanton=obtieneCantones(opcionesProvincia[i])

                var adapter01= ArrayAdapter(this@registrar_publicacion,android.R.layout.simple_list_item_1,opcionesCanton)
                cmbCanton.adapter=adapter01

                resProvincia=opcionesProvincia[i]
            }
            override fun onNothingSelected(adapterView: AdapterView<*>) {
            }

        }
        /*--------------------------------------codigo del fin de comboBox provincia y canton-------------------------------------*/


        /*--------------------------------------codigo del comboBox categoria y subcategoria--------------------------------------------*/
        var categoria:String="Plomería o Fontanería"
        val opcionesCategoria= arrayOf(
            "Plomería o Fontanería",
            "Mecanica",
            "Belleza",
            "Albañilería",
            "Carpintería",
            "Electricista",
            "Cerrajería")

        cmbCategoria = findViewById(R.id.cmbCategoria) as Spinner
        cmbSubCategoria= findViewById(R.id.cmbSubCategoria) as Spinner


        var adapter2= ArrayAdapter(this,android.R.layout.simple_list_item_1,opcionesCategoria)
        cmbCategoria.adapter=adapter2

        //LISTENER
        cmbCategoria.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {

                val opcionesSubCategoria=obtieneSubCategoria(opcionesCategoria[i])

                var adapter3= ArrayAdapter(this@registrar_publicacion,android.R.layout.simple_list_item_1,opcionesSubCategoria)
                cmbSubCategoria.adapter=adapter3

                resCategoria=opcionesCategoria[i]
            }
            override fun onNothingSelected(adapterView: AdapterView<*>) {
            }

        }
        /*--------------------------------------codigo del fin de comboBox categoria y subCategoria-------------------------------------*/
        btnRegistrarPublic.setOnClickListener {
            val prefe: SharedPreferences = getSharedPreferences("appData", Context.MODE_PRIVATE)
            val idUsuario: String? = prefe.getString("id", "")


            resCanton=cmbCanton.getSelectedItem().toString()
            resSubCategoria=cmbSubCategoria.getSelectedItem().toString()

            registraPublicacion(txtDescripcion.text.toString().trim { it <= ' ' }, resProvincia, resCanton, resCategoria, resSubCategoria, idUsuario.toString())
        }
    }

    private fun registraPublicacion(descripcion:String, provincia:String, canton:String, categoria: String, subcategoria:String, idUsuario:String){

        val idDato = fiba.push().key
        val dato = idDato?.let { clsPublicacion(it, descripcion, provincia, canton, categoria, subcategoria, idUsuario) }

        if (idDato != null) {
            fiba.child(idDato).setValue(dato).addOnCompleteListener {
                alerta("Su publicación a sido registrada satisfactoriamente", "¡Enhorabuena!")
            }
        }
    }
    fun alerta(mensaje:String, titulo:String){
        val dialogBuilder = AlertDialog.Builder(this)

        // set message of alert dialog
        dialogBuilder.setMessage(mensaje)
            // if the dialog is cancelable
            .setCancelable(false)

            // negative button text and action
            .setNegativeButton("Aceptar", DialogInterface.OnClickListener {
                    dialog, id -> dialog.cancel()
            })

        // create dialog box
        val alert = dialogBuilder.create()
        // set title for alert dialog box
        alert.setTitle(titulo)
        // show alert dialog
        alert.show()
    }


    fun obtieneCantones(nameProvincia:String): kotlin.Array<String> {
        val cantones: ArrayList<String> = ArrayList()

        if(nameProvincia=="San José"){
            cantones.add("San José")
            cantones.add("Escazú")
            cantones.add("Desamparados")
            cantones.add("Puriscal")
            cantones.add("Tarrazú")
            cantones.add("Aserrí")
            cantones.add("Mora")
            cantones.add("Goicoechea")

        }else if(nameProvincia=="Alajuela"){
            cantones.add("Alajuela")
            cantones.add("Atenas")
            cantones.add("Grecia")
            cantones.add("Guatuso")
            cantones.add("Los Chiles")
            cantones.add("Naranjo")
            cantones.add("Orotina")
            cantones.add("Palmares")

        }else if(nameProvincia=="Cartago"){
            cantones.add("Cartago")
            cantones.add("Paraíso")
            cantones.add("La Unión")
            cantones.add("Jiménez")
            cantones.add("Turrialba")
            cantones.add("Alvarado")
            cantones.add("Oreamuno")
            cantones.add("El Guarco")

        }else if(nameProvincia=="Heredia"){
            cantones.add("Heredia")
            cantones.add("San Rafael")
            cantones.add("San Isidro")
            cantones.add("Belén")
            cantones.add("Jiménez")
            cantones.add("Barva")
            cantones.add("Santo Domingo")
            cantones.add("Santa Bárbara")
            cantones.add("Flores")
            cantones.add("San Pablo")
            cantones.add("Sarapiquí")

        }else if(nameProvincia=="Guanacaste"){
            cantones.add("Liberia")
            cantones.add("Nicoya")
            cantones.add("Santa Cruz")
            cantones.add("Bagaces")
            cantones.add("Carrillo")
            cantones.add("Cañas")
            cantones.add("Abangares")
            cantones.add("Tilarán")
            cantones.add("Nandayure")
            cantones.add("La Cruz")
            cantones.add("Hojancha")

        }else if(nameProvincia=="Puntarenas"){
            cantones.add("Puntarenas")
            cantones.add("Buenos Aires")
            cantones.add("Corredores")
            cantones.add("Coto Brus")
            cantones.add("Esparza")
            cantones.add("Garabito")
            cantones.add("Golfito")
            cantones.add("Montes de Oro")
            cantones.add("Osa")
            cantones.add("Parrita")
            cantones.add("Quepos")

        }else if(nameProvincia=="Limón"){
            cantones.add("Limón")
            cantones.add("Guácimo")
            cantones.add("Matina")
            cantones.add("Pococí")
            cantones.add("Siquirres")
            cantones.add("Talamanca")
        }
        return cantones.toTypedArray()
    }
    fun obtieneSubCategoria(categoria:String): kotlin.Array<String> {
        val subcategoria: ArrayList<String> = ArrayList()

        if(categoria=="Plomería o Fontanería"){
            subcategoria.add("Reparacion eh instalación de tuberías en exteriores")
            subcategoria.add("Reparacion eh instalación de tubos en interiores")
            subcategoria.add("Reparacion eh instalación  de tuberías en interiores y exteriores")

        }else if(categoria=="Mecanica"){
            subcategoria.add("Mecanico de motocicletas")
            subcategoria.add("Mecanico de carros")
            subcategoria.add("Mecanico de carros y motocicletas")
            subcategoria.add("Electricista de carros")
            subcategoria.add("Electricista de motocicletas")
            subcategoria.add("Electricista de carros y mocicletas")

        }else if(categoria=="Belleza"){
            subcategoria.add("Corte de cabello (hombre)")
            subcategoria.add("Corte de cabello (mujer)")
            subcategoria.add("Corte de cabello(hombre y mujer)")
            subcategoria.add("Arreglo de uñas")
            subcategoria.add("Maquillista")

        }else if(categoria=="Albañilería"){
            subcategoria.add("Pintor")
            subcategoria.add("Contrucción")

        }else {
            subcategoria.add("No hay subcategorías disponibles")
        }
        return subcategoria.toTypedArray()
    }

}