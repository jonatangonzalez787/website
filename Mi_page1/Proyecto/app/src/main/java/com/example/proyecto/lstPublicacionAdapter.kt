package com.example.proyecto

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

class lstPublicacionAdapter(context: Context, val pantalla: Int, val lstPersona: List<clsPublicacion>) :
    ArrayAdapter<clsPublicacion>(context, pantalla, lstPersona){

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layoutInflater: LayoutInflater = LayoutInflater.from(context)
        val view: View = layoutInflater.inflate(pantalla, null )

        val dato =lstPersona[position]

        val txvId = view.findViewById<TextView>(R.id.txvtId)
        val txvNombre = view.findViewById<TextView>(R.id.txvNombre)
        val txvEmail = view.findViewById<TextView>(R.id.txvEmail)

        txvId.text = dato.id
        txvNombre.text = dato.provincia
        txvEmail.text = dato.subcategoria

        return view
    }
}