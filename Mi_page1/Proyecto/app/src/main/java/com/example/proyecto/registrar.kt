package com.example.proyecto

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_registrar.*

class registrar: AppCompatActivity() {
    // Create local variables but not initialized
    lateinit var fiba: DatabaseReference
    lateinit var lstdatos: MutableList<clsUsuario>
    lateinit var token: String


    override fun onCreate(savedInstanceState: Bundle?) {
        fiba = FirebaseDatabase.getInstance().getReference("usuario")
        lstdatos = mutableListOf()
        token = ""

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registrar)

        btnCrear.setOnClickListener {
            if (txtNombre.text.toString().trim().isEmpty()) {
                txtNombre.error = "El nombre es requerido"

            } else if (txtTelefono.text.toString().trim().isEmpty()) {
                txtTelefono.error = "El telefono es requerido"

            } else if (txtCorreo.text.toString().trim().isEmpty()) {
                txtCorreo.error = "El correo es requerido"

            } else if (txtContra.text.toString().trim().isEmpty()) {
                txtContra.error = "La contraseña es requerida"

            } else {
                insertarBD(txtNombre.text.toString().trim(),
                           txtTelefono.text.toString().trim(),
                           txtCorreo.text.toString().trim(),
                           txtContra.text.toString().trim())

                Toast.makeText(applicationContext, "Su usuario a sido registrado correctamente", Toast.LENGTH_LONG).show()

                var intent: Intent = Intent(this, login::class.java)
                startActivity(intent)
            }

        }
    }
    //Inserta en la base de datos de firebase
    private fun insertarBD(nombre: String, telefono: String, correo: String, email: String) {
        val idDato = fiba.push().key
        val dato = idDato?.let { clsUsuario(it, nombre, telefono, correo, email) }

        if (idDato != null) {
            fiba.child(idDato).setValue(dato).addOnCompleteListener {
                alerta("Su usuario a sido registrado satisfactoriamente", "¡Enhorabuena!")
            }
        }
    }

    fun alerta(mensaje:String, titulo:String){
        val dialogBuilder = AlertDialog.Builder(this)

        // set message of alert dialog
        dialogBuilder.setMessage(mensaje)
            // if the dialog is cancelable
            .setCancelable(false)

            // negative button text and action
            .setNegativeButton("Aceptar", DialogInterface.OnClickListener {
                    dialog, id -> dialog.cancel()
            })

        // create dialog box
        val alert = dialogBuilder.create()
        // set title for alert dialog box
        alert.setTitle(titulo)
        // show alert dialog
        alert.show()
    }
}