package com.example.proyecto

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_main.*
import java.util.ArrayList

class MainActivity : AppCompatActivity() {
    lateinit var cmbProvincia:Spinner
    lateinit var cmbCanton:Spinner

    lateinit var cmbCategoria:Spinner
    lateinit var cmbSubCategoria:Spinner

    lateinit var resCategoria: String
    lateinit var resSubCategoria: String
    lateinit var resProvincia: String
    lateinit var resCanton: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        /*--------------------------------------codigo del comboBox provincia y canton--------------------------------------------*/
        var provincia:String="San José"
        val opcionesProvincia= arrayOf(
            "Todos",
            "San José",
            "Alajuela",
            "Cartago",
            "Heredia",
            "Guanacaste",
            "Puntarenas",
            "Limón")

        cmbProvincia = findViewById(R.id.cmbProvincia) as Spinner
        cmbCanton = findViewById(R.id.cmbCanton) as Spinner


        var adapter= ArrayAdapter(this,android.R.layout.simple_list_item_1,opcionesProvincia)
        cmbProvincia.adapter=adapter

        //LISTENER
        cmbProvincia.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {

                val opcionesCanton=obtieneCantones(opcionesProvincia[i])

                var adapter01= ArrayAdapter(this@MainActivity,android.R.layout.simple_list_item_1,opcionesCanton)
                cmbCanton.adapter=adapter01

                resProvincia=opcionesProvincia[i]
            }
            override fun onNothingSelected(adapterView: AdapterView<*>) {
            }

        }


        /*--------------------------------------codigo del fin de comboBox provincia y canton-------------------------------------*/


        /*--------------------------------------codigo del comboBox categoria y subcategoria--------------------------------------------*/
        var categoria:String="Plomería o Fontanería"
        val opcionesCategoria= arrayOf(
            "Todos",
            "Plomería o Fontanería",
            "Mecanica",
            "Belleza",
            "Albañilería",
            "Carpintería",
            "Electricista",
            "Cerrajería")

        cmbCategoria = findViewById(R.id.cmbCategoria) as Spinner
        cmbSubCategoria= findViewById(R.id.cmbSubCategoria) as Spinner


        var adapter2= ArrayAdapter(this,android.R.layout.simple_list_item_1,opcionesCategoria)
        cmbCategoria.adapter=adapter2

        //LISTENER
        cmbCategoria.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {

                val opcionesSubCategoria=obtieneSubCategoria(opcionesCategoria[i])

                var adapter3= ArrayAdapter(this@MainActivity,android.R.layout.simple_list_item_1,opcionesSubCategoria)
                cmbSubCategoria.adapter=adapter3

                resCategoria=opcionesCategoria[i]
            }
            override fun onNothingSelected(adapterView: AdapterView<*>) {
            }

        }
        /*--------------------------------------codigo del fin de comboBox categoria y subCategoria-------------------------------------*/
        btnRegistrarPublic_MA.setOnClickListener {
            var intent2: Intent = Intent(this, registrar_publicacion::class.java)
            startActivity(intent2)
        }

        btnBuscar.setOnClickListener {
            resCanton=cmbCanton.getSelectedItem().toString()
            resSubCategoria=cmbSubCategoria.getSelectedItem().toString()



            var intent2: Intent = Intent(this, lista_publicaciones::class.java)
            intent2.putExtra("provincia", resProvincia)
            intent2.putExtra("canton", resCanton)
            intent2.putExtra("categoria", resCategoria)
            intent2.putExtra("subcategoria", resSubCategoria)

            startActivity(intent2)
        }
    }

    override fun onStart() {
        super.onStart()
        //el nombre puede ser cualquiera pero esta linea es la que me va a permitir a mi poder tener el localSotage
        val prefe: SharedPreferences = getSharedPreferences("appData", Context.MODE_PRIVATE)
        val usuario: String? = prefe.getString("correo", "")

        if(usuario.toString().trim().length==0){
            var intent: Intent = Intent(this, login::class.java)
            startActivity(intent)
            finish()
            //Toast.makeText(this, "sin usuarios", Toast.LENGTH_SHORT).show();
        }
    }


    fun obtieneCantones(nameProvincia:String): kotlin.Array<String> {
        val cantones: ArrayList<String> = ArrayList()
        cantones.add("Todos")

        if(nameProvincia=="San José"){
            cantones.add("San José")
            cantones.add("Escazú")
            cantones.add("Desamparados")
            cantones.add("Puriscal")
            cantones.add("Tarrazú")
            cantones.add("Aserrí")
            cantones.add("Mora")
            cantones.add("Goicoechea")

        }else if(nameProvincia=="Alajuela"){
            cantones.add("Alajuela")
            cantones.add("Atenas")
            cantones.add("Grecia")
            cantones.add("Guatuso")
            cantones.add("Los Chiles")
            cantones.add("Naranjo")
            cantones.add("Orotina")
            cantones.add("Palmares")

        }else if(nameProvincia=="Cartago"){
            cantones.add("Cartago")
            cantones.add("Paraíso")
            cantones.add("La Unión")
            cantones.add("Jiménez")
            cantones.add("Turrialba")
            cantones.add("Alvarado")
            cantones.add("Oreamuno")
            cantones.add("El Guarco")

        }else if(nameProvincia=="Heredia"){
            cantones.add("Heredia")
            cantones.add("San Rafael")
            cantones.add("San Isidro")
            cantones.add("Belén")
            cantones.add("Jiménez")
            cantones.add("Barva")
            cantones.add("Santo Domingo")
            cantones.add("Santa Bárbara")
            cantones.add("Flores")
            cantones.add("San Pablo")
            cantones.add("Sarapiquí")

        }else if(nameProvincia=="Guanacaste"){
            cantones.add("Liberia")
            cantones.add("Nicoya")
            cantones.add("Santa Cruz")
            cantones.add("Bagaces")
            cantones.add("Carrillo")
            cantones.add("Cañas")
            cantones.add("Abangares")
            cantones.add("Tilarán")
            cantones.add("Nandayure")
            cantones.add("La Cruz")
            cantones.add("Hojancha")

        }else if(nameProvincia=="Puntarenas"){
            cantones.add("Puntarenas")
            cantones.add("Buenos Aires")
            cantones.add("Corredores")
            cantones.add("Coto Brus")
            cantones.add("Esparza")
            cantones.add("Garabito")
            cantones.add("Golfito")
            cantones.add("Montes de Oro")
            cantones.add("Osa")
            cantones.add("Parrita")
            cantones.add("Quepos")

        }else if(nameProvincia=="Limón"){
            cantones.add("Limón")
            cantones.add("Guácimo")
            cantones.add("Matina")
            cantones.add("Pococí")
            cantones.add("Siquirres")
            cantones.add("Talamanca")
        }
        return cantones.toTypedArray()
    }
    fun obtieneSubCategoria(categoria:String): kotlin.Array<String> {
        val subcategoria: ArrayList<String> = ArrayList()
        subcategoria.add("Todos")

        if(categoria=="Plomería o Fontanería"){
            subcategoria.add("Reparacion eh instalación de tuberías en exteriores\n")
            subcategoria.add("Reparacion eh instalación de tubos en interiores")
            subcategoria.add("Reparacion eh instalación  de tuberías en interiores y exteriores")

        }else if(categoria=="Mecanica"){
            subcategoria.add("Mecanico de motocicletas")
            subcategoria.add("Mecanico de carros")
            subcategoria.add("Mecanico de carros y motocicletas")
            subcategoria.add("Electricista de carros")
            subcategoria.add("Electricista de motocicletas")
            subcategoria.add("Electricista de carros y mocicletas")

        }else if(categoria=="Belleza"){
            subcategoria.add("Corte de cabello (hombre)")
            subcategoria.add("Corte de cabello (mujer)")
            subcategoria.add("Corte de cabello(hombre y mujer)")
            subcategoria.add("Arreglo de uñas")
            subcategoria.add("Maquillista")

        }else if(categoria=="Albañilería") {
            subcategoria.add("Pintor")
            subcategoria.add("Contrucción")
        }else if(categoria=="Todos"){
            subcategoria.clear()
            subcategoria.add("Todos")

        }else {
            subcategoria.clear()
            subcategoria.add("No hay subcategorías disponibles")
        }
            return subcategoria.toTypedArray()
        }

}