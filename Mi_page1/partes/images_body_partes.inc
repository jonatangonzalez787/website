<style>
    .cls_background {
        /* BACKGROUND DE TODOS LOS DIV MAIN MENOS EL PRIMERO"imagenes iglesia" */
        background: #1c1c1c;
    }

    .row {
        margin-right: 0px;
        margin-left: 0px;
        padding: 15px
    }

    .cls_images_body_main {
        width: 100%;
        height: 400px
    }

    .cls_images_body {
        width: 100%;
        height: 200px
    }

    .cls_div_body_images {
        /* padding: 60px; */
        padding: 20px;
        color: white;
        background: #121212;
        /* BACKGROUND DE PAGINA FONDO MAIN */
        font-family: "Segoe UI", Helvetica, Arial, sans-serif;
        display: block;
    }

    hr {
        border-top: 1px solid #555
    }

    .cls_row {
        padding: 0px;
        /* background: #1c1c1c */
    }

    .cls_row2 {
        padding: 30px;
        background: #121212
    }

    .cls_row3 {
        padding: 15x
    }

    .cls_btn_img {
        width: 100%;
        text-align: center;
        padding: 5px;
        background: #252525
            /* background: linear-gradient(220deg, #0a2a66, #0a2a66); */
    }

    .cls_btn_img2 {
        width: 100%;
        padding: 5px;
        background: #202020
    }

    @media (max-width:768px) {
        .cls_images_body_main {
            width: 100%;
            height: 200px
        }

        .cls_images_body {
            height: 150px
        }

    }

    @media (max-width:768px) {

        .cls_div_body_images {
            /* padding-left: 0px; */
            padding: 20px;
        }

        .cls_row2 {
            padding: 15px
        }

        .cls_row3 {
            padding: 0px
        }

        .cls_background {
            background: #202020;
        }
    }

    @media (max-width:475px) {
        .cls_div_body_images {
            /* padding-left: 0px; */
            padding: 0px;
        }

    }

    .contenedor img {
        transition: all 1s;
    }

    .contenedor img:hover {
        -webkit-transform: scale(1.3);
        transform: scale(1.2);

    }

    .contenedor {
        overflow: hidden;
    }

    /* .contenedor {overflow:hidden;}
    .contenedor img:hover {-webkit-transform:scale(1.3);transform:scale(1.3);} */
</style>


<div class="cls_div_body_images">


    <!-- <br><br> -->
    <!-- <div style="padding: 30px;background: #202020"> -->

    <div class="cls_background cls_row">

        <div class="cls_row2">
            <h1 style="color: #ffffff">Imágenes de la Iglesia</h1>
            <p id="imagenes" style="color: #ffffff">Conjunto de imágenes que busqué en internet y aquí muestro algunas
                que escogí.</p>
            <i style="color: gray">Sábado 3 mayo del 2020</i>
            <hr style="color: #333">

        </div>
        <!-- <br><br> -->

        <div class="row" style="width: 100%;margin: auto">


            <div style="padding:0px" class="col-xs-12 col-sm-12 col-md-6">
                <!-- <div style="position: absolute;top:50%;left: 25%">
                    <strong>Templo Sede Internacionl LLDM</strong>

                </div> -->
                <a href="index.php"> <img class="cls_images_body_main" src="https://www.dropbox.com/s/v48g4j8ew4zvueq/honorable2.jpg?dl=1" alt="ima"></a>

            </div>
            <!-- ------------------------------------------------------------------------------ -->
            <div style="padding:0px" class="col-xs-6 col-sm-6 col-md-3">
                <div class="contenedor" style="padding:0px" class="col-xs-12 col-sm-12 col-md-12">
                    <img id="img1" title="templo" class="cls_images_body" src="https://www.dropbox.com/s/2wvo8cu2puoq8jw/luz-1.jpg?dl=1" alt="ima">

                </div>
                <div class="contenedor" style="padding:0px" class="col-xs-12 col-sm-12 col-md-12">
                    <img id="img2" class="cls_images_body" src="https://www.dropbox.com/s/iiu0ueljw23sna9/boni.jpg?dl=1" alt="ima">
                </div>
            </div>
            <div style="padding:0px" class="col-xs-6 col-sm-6 col-md-3">
                <div class="contenedor" style="padding:0px" class="col-xs-12 col-sm-12 col-md-12">
                    <img id="img3" class="cls_images_body" src="https://www.dropbox.com/s/6c4nkmnd1ny76k4/caminata.jpg?dl=1" alt="ima">
                </div>
                <div class="contenedor" style="padding:0px" class="col-xs-12 col-sm-12 col-md-12">
                    <img id="img4" class="cls_images_body" src="https://www.dropbox.com/s/qi7x11ntwsa8ksh/for.jpg?dl=1" alt="ima">
                </div>
            </div>

        </div>
        <br>

        <!-- ---------------------------------------buttons---------------------------------------------------- -->
        <div class="row cls_btn_img">
            <!-- <div  class="row cls_btn_img2"> -->
            <button onclick="myFunction1()" class="button"><span>Templos </span></button>
            <button onclick="myFunction2()" class="button"><span>Caminata</span></button>
            <button onclick="myFunction3()" class="button"><span>Otros </span></button>
            <!-- </div> -->
        </div>

        <!-- -------------------------------------ROW2-------------------------------------------------------------- -->
        <div class="row cls_row3">



            <div class="col-sm-12 col-md-6">
                <div style="width: 100%;display: inline-table">
                    <h1>Iglesia La Luz del Mundo</h1>
                    <hr>
                    <h3>Isaias 43: 4</h3>
                    <i>
                        <p>
                            "Porque a mis ojos fuiste de gran estima, fuiste honorable, y yo te amé;
                            daré, pues, hombres por ti, y naciones por tu vida."
                            <p style="color: gray"> #Honorable </p>
                        </p>
                    </i>
                </div>

            </div>

            <div class="col-sm-12 col-md-6">
                <div style=" height: 200px;display: inline-table">

                    <div>
                        <h3>Galería de imágenes</h3>
                        <hr>
                        <i>
                            <p>
                                Ésta es una recopilación de imágenes de templos de la Iglesia, caminata de jóvenes
                                y algunas otras más.
                                <p style="color: gray"> #GalleryLLDM</p>
                            </p>
                        </i>
                        <!-- <div class="row cls_btn_img">

                            <button class="button"><span>Templos </span></button>
                            <button class="button"><span>Caminata</span></button>
                            <button class="button"><span>Otros </span></button>

                        </div> -->
                    </div>
                </div>
            </div>

        </div><!-- fin 2do ROW -->

    </div>
    <br>

</div>


<style>
    .button {
        /* border-radius: 1px; */
        border-radius: 13px;
        background-image: 'https://www.dropbox.com/s/z4c6gwho9ph7len/lldm2.jpg?dl=1';
        background-color: #202020;
        /* background: linear-gradient(220deg, #0a2a66, #0a2a66); */
        /*  background: linear-gradient(180deg, #0A2AB2, #0a2a66); */
        width: 29%;

        /* border-radius: 1px; */
        /* background-color: #202020; */
        /* border: none; */
        border: 0.5px solid #4f4f4f;
        color: white;
        text-align: center;
        font-size: 18px;
        padding: 2px;
        /* width: 100px; */
        transition: all 0.5s;
        cursor: pointer;
        margin: 5px;
    }

    .button span {
        cursor: pointer;
        display: inline-block;
        position: relative;
        transition: 0.5s;

        font-size: 15px;
        font-family: Montserrat;
    }

    .button span:after {
        content: '\00bb';
        position: absolute;
        opacity: 0;
        top: 0;
        right: -20px;
        transition: 0.5s;
    }

    .button:hover span {
        padding-right: 25px;
    }

    .button:hover span:after {
        opacity: 1;
        right: 0;
    }

    @media (min-width:1370px) {
        .button {
            width: 200px
        }

        .button:hover span {
            padding-right: 5px;
        }

    }
</style>

<script>
    function myFunction1() {
        document.getElementById('img1').src = "https://www.dropbox.com/s/zp6x07tw6lftisl/sila2.jpg?dl=1";
        document.getElementById('img2').src = "https://www.dropbox.com/s/usyqhhe42ghfdsr/sil.jpg?dl=1";
        document.getElementById('img3').src = "https://www.dropbox.com/s/ng5k5cth39vy1nt/lldmii.jpg?dl=1";
        document.getElementById('img4').src = "https://www.dropbox.com/s/z4c6gwho9ph7len/lldm2.jpg?dl=1";
    }

    function myFunction2() {
        document.getElementById('img1').src = "https://www.dropbox.com/s/6c4nkmnd1ny76k4/caminata.jpg?dl=1";
        document.getElementById('img2').src = "https://www.dropbox.com/s/u5d3t0jth3v8frd/calz.jpg?dl=1";
        document.getElementById('img3').src = "https://www.dropbox.com/s/r2dd1bdcqg47clf/lldmi.jpg?dl=1";
        document.getElementById('img4').src = "https://www.dropbox.com/s/hmvnmyv2prrc2z7/lldm3.jpg?dl=1";
    }
</script>